import sys
sys.path.append(sys.path[0]+'/../python_environment/MambaEnv/lib/python3.11/site-packages')
import torch,os,json,time,argparse
from torch.distributed.fsdp import FullyShardedDataParallel as FSDP
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.distributed import init_process_group, destroy_process_group
from mamba_ssm import Mamba

def display_results(res):
    separator = "|"
    for i in range(len(res["header"])):
        maxi = max([len(line[i]) for line in res["lines"]])
        separator += ":"
        if len(res["header"][i]) > maxi:
            separator += "-"*(len(res["header"][i])-2)
        else:
            separator += "-"*(maxi-2)
        separator += ":|"
    margins = [len(i) for i in separator.split("|")[1:-1]]
    text = ""
    text += "|"+"|".join([res["header"][i]+(margins[i]-len(res["header"][i]))*" " for i in range(len(margins))])+"|"+"\n"
    text += separator+"\n"
    for line in res["lines"]:
        text += "|"+"|".join([line[i]+(margins[i]-len(line[i]))*" " for i in range(len(margins))])+"|"+"\n"
    return text

def merge_results(res1,res2):
    res = {}
    assert res1["header"] == res2["header"]
    res["header"] = [header for header in res1["header"]]
    res["lines"] = [[value for value in line] for line in res1["lines"]]+[[value for value in line] for line in res2["lines"]]
    return res

def clean_result_line(line):
    return [",".join(str(value).split(".")) for value in line]

def print_one(text):
    global local_rank
    if local_rank == 0:
        print(text)

def model_size(model):
    return f"{sum(p.numel() for p in model.parameters()) / 1e6:.0f}"

def benchmark_inference(model, batch, prompt_length, generation_length, model_dim, device="cuda", warm_up=5, samples=5):
    global local_rank
    model.eval()
    prompts = torch.randn(batch, prompt_length, model_dim).to(device)

    for _ in range(warm_up):
        with torch.no_grad():
            output = model(prompts[:, :generation_length, :])

    total_time = 0.0
    for _ in range(samples):
        start_time = time.time()
        with torch.no_grad():
            output = model(prompts[:, :generation_length, :])
        end_time = time.time()
        total_time += end_time - start_time

    avg_time = total_time / samples
    throughput = (batch * generation_length) / avg_time
    return avg_time, throughput

def benchmark_training(model, batch, prompt_length, generation_length, model_dim, device="cuda", warm_up=5, samples=5):
    model.train()
    prompts = torch.randn(batch, prompt_length, model_dim).to(device)
    targets = torch.randn(batch, prompt_length, model_dim).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-4)

    for _ in range(warm_up):
        optimizer.zero_grad()
        output = model(prompts[:, :generation_length, :])
        loss = torch.nn.functional.mse_loss(output, targets[:, :generation_length, :])
        loss.backward()
        optimizer.step()

    total_time = 0.0
    for _ in range(samples):
        start_time = time.time()
        optimizer.zero_grad()
        output = model(prompts[:, :generation_length, :])
        loss = torch.nn.functional.mse_loss(output, targets[:, :generation_length, :])
        loss.backward()
        optimizer.step()
        end_time = time.time()
        total_time += end_time - start_time

    avg_time = total_time / samples
    throughput = (batch * generation_length) / avg_time
    return avg_time, throughput

parser = argparse.ArgumentParser(description="Benchmark Mamba 1 Model")
parser.add_argument("--mode", type=str, choices=["training", "inference"], default="training", help="training or inference")
parser.add_argument("--batch", type=int, default=1, help="Batch size")
parser.add_argument("--d_model", type=int, default=10000, help="Model dimension")
parser.add_argument("--prompt_length", type=int, default=2048, help="Length of random prompt")
parser.add_argument("--generation_length", type=int, default=128, help="Generation length")
parser.add_argument("--distribution", type=str, choices=["FSDP", "DDP"], default="FSDP", help="Type of distribution : FSDP or DDP")
args = parser.parse_args()

torch.distributed.init_process_group(backend='nccl', init_method='env://')
local_rank = torch.distributed.get_rank()
gpus = torch.distributed.get_world_size()
torch.cuda.set_device(local_rank%8)
device = torch.device("cuda", local_rank%8)

headers = ["Mode","Distribution","Batch","Model_dimension","Parameters","Time","Throughput"]

results = {
    "header":[h for h in headers],
    "lines":[]
}

print_one(f"\nInitializing Mamba 1 Model with model dimension of {args.d_model} on {gpus} gpus...")

model = Mamba(args.d_model).to(device)

parameters = model_size(model)

print_one(f"Model initialized : {parameters}m parameters.")

if args.distribution == "FSDP":
    print_one("Setting up Fully Sharded Data Parallel...")
    model = FSDP(model)
elif args.distribution == "DDP":
    print_one("Setting up Distributed Data Parallel...")
    model = DDP(model, device_ids=[local_rank])

print_one("FullyShardedDataParallel initialized.")
print_one("Starting benchmarks...")

print_one(f"Benchmarking Batch Size: {args.batch}...")

if args.mode == "training":
    run_time, run_throughput = benchmark_training(model, args.batch, args.prompt_length, args.generation_length, args.d_model, device)
elif args.mode == "inference":
    run_time, run_throughput = benchmark_inference(model, args.batch, args.prompt_length, args.generation_length, args.d_model, device)

results["lines"].append(clean_result_line([args.mode,args.distribution,args.batch,args.d_model,parameters,run_time,run_throughput]))

print_one(f"Memory cleaning...")
del model
torch.cuda.empty_cache()

destroy_process_group()

if local_rank == 0:
    print("\n --------------------============ Result ============--------------------\n")
    print(display_results(results))
    print(" ---------------------------------------------------------------------------")

    filename = "benchmarks.json"

    if not os.path.isfile(filename):
        json_data = {
            "header":[h for h in headers],
            "lines":[]
        }
    else:
        with open(filename, 'r') as f:
            try:
                json_data = json.load(f)
            except json.JSONDecodeError:
                json_data = {
                    "header":[h for h in headers],
                    "lines":[]
                }

    merged = merge_results(json_data,results)

    with open(filename, 'w') as f:
        json.dump(merged, f)
