#!/bin/bash
#SBATCH --account=ACCOUNT
#SBATCH --job-name=MambaBenchmark
#SBATCH --constraint=MI250
#SBATCH --nodes=1
#SBATCH --exclusive
#SBATCH --time=00:10:00
##SBATCH --reservation=xcross

rm -rf $HOMEDIR/.triton

source MambaEnv.sh

export HF_HOME="${SCRATCHDIR}/USER_DSDIR"
export HF_HUB_OFFLINE=1

export MIOPEN_USER_DB_PATH="/tmp/${USER}-miopen-cache-${SLURM_JOB_ID}"
export MIOPEN_CUSTOM_CACHE_DIR="${MIOPEN_USER_DB_PATH}"

nohup sh Analysis/collector.sh "data-$SLURM_JOB_ID.out" &
collector_pid=$!

srun --ntasks-per-node=1 --gpus-per-task=8 --cpu-bind=none --mem-bind=none --label \
    -- torchrun --nnodes="${SLURM_NNODES}" --nproc_per_node=8 \
    --rdzv-id="${SLURM_JOBID}" \
    --rdzv-backend=c10d \
    --rdzv-endpoint="$(scontrol show hostname ${SLURM_NODELIST} | head -n 1):29400" \
    --max-restarts="0" \
    -- Benchmarks/TremouletBenchmark-2.0.py "$@"

kill $collector_pid
