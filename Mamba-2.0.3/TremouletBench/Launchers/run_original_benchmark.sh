#!/bin/bash
#SBATCH --account=ACCOUNT
#SBATCH --constraint=MI250
#SBATCH --job-name="MambaBenchmark"
#SBATCH --time=00:10:00
#SBATCH --nodes=1
#SBATCH --gpus-per-node=1
#SBATCH --ntasks-per-node=1
#SBATCH --exclusive

source MambaEnv.sh

rm -rf $HOMEDIR/.triton
srun --gpu-bind=closest --cpu-bind=none --mem-bind=none python3 Benchmarks/original_benchmark.py "$@"
