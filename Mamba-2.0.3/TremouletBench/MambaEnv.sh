echo
echo "[Mamba environment initialization]"

echo "Setting up the modules..."

module purge

module load PrgEnv-gnu

module load cray-python/3.11.5
module load torch/2.2.1

module load cray-mpich
module load aws-ofi-rccl

pip install --upgrade pip virtualenv packaging
ENV_PATH="./python_environment/MambaEnv"
if [ -d "$ENV_PATH" ]; then
  echo "Use of a python environment... (python_environment/MambaEnv)"
  source "$ENV_PATH/bin/activate"
else
  echo "Creating the python environment..."
  python3 -m virtualenv "$ENV_PATH"
  chmod +x "$ENV_PATH/bin/activate"
  source "$ENV_PATH/bin/activate"
fi
pip install --upgrade pip virtualenv packaging

pip install certifi==2024.2.2
pip install filelock==3.13.1
pip install fsspec==2024.2.0
pip install huggingface-hub==0.23.2
pip install Jinja2==3.1.3
pip install networkx==3.2.1
pip install numpy==1.26.3
pip install platformdirs==4.2.1
pip install sympy==1.12
pip install typing_extensions==4.9.0

pip install transformers==4.41.2
pip install accelerate==0.30.1
pip install protobuf==5.27.0
pip install sentencepiece==0.2.0

pip install /lus/work/BCINES/dci/SHARED/bench-ai/Mamba/wheels/causal_conv1d/1.2.0.post2/causal_conv1d-1.2.0.post2-cp311-cp311-linux_x86_64.whl
pip install /lus/work/BCINES/dci/SHARED/bench-ai/Mamba/wheels/mamba/2.0.3/mamba_ssm-2.0.3-cp311-cp311-linux_x86_64.whl

echo "[End of mamba environment initialization]"
echo
