#!/bin/bash

#SBATCH --account=ACCOUNT
#SBATCH --job-name="MambaRocmInferenceBenchmark"
#SBATCH --constraint=MI250
#SBATCH --nodes=1
#SBATCH --exclusive
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --gpus-per-node=1
#SBATCH --time=01:00:00

source MambaEnv.sh

if echo $@ | grep -q "model-name"; then
  if echo $@ | grep -q "state-spaces/mamba-"; then
    rm -rf $HOMEDIR/.triton
  fi
else
  rm -rf $HOMEDIR/.triton
fi

srun \
  --account=ACCOUNT \
  --job-name="MambaRocmInferenceBenchmark" \
  --constraint=MI250 \
  --nodes=1 \
  --exclusive \
  --ntasks-per-node=1 \
  --cpus-per-task=1 \
  --gpus-per-node=1 \
  --time=01:00:00 \
  -- python InferenceBenchmark.py "$@"
