import torch,os,time,argparse
from numpy import mean,var
from mamba_ssm.models.mixer_seq_simple import MambaLMHeadModel
from transformers import AutoTokenizer, AutoModelForCausalLM

parser = argparse.ArgumentParser(description="Generation benchmarking")
parser.add_argument("--model-name", type=str, default="state-spaces/mamba-130m")
parser.add_argument("--model-path", type=str, default="/lus/work/BCINES/dci/SHARED/bench-ai/Mamba/models")
parser.add_argument("--prompt", type=str, default=None)
parser.add_argument("--promptlen", type=int, default=2048)
parser.add_argument("--genlen", type=int, default=128)
parser.add_argument("--temperature", type=float, default=1.0)
parser.add_argument("--topk", type=int, default=1)
parser.add_argument("--topp", type=float, default=1.0)
parser.add_argument("--minp", type=float, default=0.0)
parser.add_argument("--repetition-penalty", type=float, default=1.0)
parser.add_argument("--batch", type=int, default=1)
args = parser.parse_args()

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
repeats = 3
dtype = torch.float16

is_mamba = args.model_name.startswith("state-spaces/mamba-")

model_name = os.path.join(args.model_path,args.model_name)
tokenizer_name = os.path.join(args.model_path,"EleutherAI/gpt-neox-20b") if is_mamba else model_name

if not os.path.isdir(model_name) or not os.path.isdir(tokenizer_name):
    print("Error: Model not downloaded.")
    exit(1)

model = MambaLMHeadModel.from_pretrained(model_name, dtype=dtype) if is_mamba else AutoModelForCausalLM.from_pretrained(model_name, torch_dtype=dtype)
tokenizer = AutoTokenizer.from_pretrained(tokenizer_name)

model.to(device)

model.eval()

torch.random.manual_seed(0)
if args.prompt is None:
    input_ids = torch.randint(1, 1000, (args.batch, args.promptlen), dtype=torch.long, device="cuda")
    attn_mask = torch.ones_like(input_ids, dtype=torch.long, device="cuda")
else:
    tokens = tokenizer(args.prompt, return_tensors="pt")
    input_ids = tokens.input_ids.to(device=device)
    attn_mask = tokens.attention_mask.to(device=device)
max_length = input_ids.shape[1] + args.genlen

if is_mamba:
    fn = lambda: model.generate(
        input_ids=input_ids,
        max_length=max_length,
        cg=True,
        return_dict_in_generate=True,
        temperature=args.temperature,
        top_k=args.topk,
        top_p=args.topp,
        min_p=args.minp,
        repetition_penalty=args.repetition_penalty,
    )
else:
    fn = lambda: model.generate(
        input_ids=input_ids,
        attention_mask=attn_mask,
        max_length=max_length,
        return_dict_in_generate=True,
        pad_token_id=tokenizer.eos_token_id,
        do_sample=True,
        temperature=args.temperature,
        top_k=args.topk,
        top_p=args.topp,
        repetition_penalty=args.repetition_penalty,
    )

times = []
output = fn()
for _ in range(repeats):
    torch.cuda.synchronize()
    start = time.time()
    fn()
    torch.cuda.synchronize()
    end = time.time()
    elapsed_time = end - start
    times.append(elapsed_time)

total_time = sum(times)
total_tokens = args.batch * args.genlen * repeats
throughput = (args.batch*args.genlen*repeats)/total_time
avg_time_per_token = total_time / total_tokens
avg_time_per_sample = mean(times)
variance = var(times)
gpu_name = torch.cuda.get_device_name(0)
pt_version = torch.__version__
gpu_memory_allocated = torch.cuda.memory_allocated(0)
num_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
num_params_billion = num_params / 1e9
model_name = args.model_name
n_gpu = torch.cuda.device_count()
generated_sequences = output.sequences.cpu().numpy()
decoded_texts = []
for seq in generated_sequences:
    generated_part = seq[len(input_ids[0]):]
    text = tokenizer.decode(generated_part)
    decoded_texts.append("".join(str(list(text)).split("', '"))[2:-2])

print(f"GPU name: {gpu_name}")
print(f"Number of GPUs : {n_gpu}")
print(f"GPU memory allocated: {gpu_memory_allocated / 1024**2:.0f} MB")
print(f"PyTorch version: {pt_version}")
print(f"Model name: {model_name}")
print(f"Number of parameters: {num_params_billion:.2f}B")
print(f"Batch size : {args.batch}")
print(f"Prompt length: {len(input_ids[0])}")
print(f"Generation length: {len(output.sequences[0]) - len(input_ids[0])}")
print(f"Throughput: {throughput:.0f} tokens/s")
print(f"Average time per token: {avg_time_per_token * 1000:.2f}ms")
print(f"Average time per sample: {avg_time_per_sample * 1000:.2f}ms")
print(f"Samples variance: {variance * 1000000:.0f}ns")
print(f"Prompt processing + decoding time: {total_time * 1000:.0f}ms")
if not args.prompt is None:
    for i, text in enumerate(decoded_texts):
        print(f"Generated sample {i + 1}: {text}")
