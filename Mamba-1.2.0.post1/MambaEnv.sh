echo
echo "[Mamba environment initialization]"

echo "Setting up the modules..."

module purge

module load PrgEnv-gnu

module load cray-python/3.11.5
module load torch/2.2.1

module load aws-ofi-rccl
module load cray-mpich

ENV_PATH="./python_environment/MambaRocmEnv"
if [ -d "$ENV_PATH" ]; then
  echo "Use of a python environment... (python_environment/MambaRocmEnv)"
  source "$ENV_PATH/bin/activate"
else
  echo "Creating the python environment..."
  python3 -m virtualenv "$ENV_PATH"
  chmod +x "$ENV_PATH/bin/activate"
  source "$ENV_PATH/bin/activate"
fi
pip install --upgrade pip
python3 -m pip install --upgrade pip
pip install --upgrade pip /lus/work/BCINES/dci/SHARED/bench-ai/Mamba/wheels/mamba/1.2.0.post1/mamba_ssm-1.2.0.post1-cp311-cp311-linux_x86_64.whl

echo "[End of mamba environment initialization]"
echo
