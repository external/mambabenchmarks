# **Mamba 1.2.0 on Adastra**

The functional Mamba module at CINES is [a fork](https://github.com/EmbeddedLLM/mamba-rocm) of Mamba edited by [kliuae](https://github.com/kliuae) named [**mamba-rocm**](https://github.com/EmbeddedLLM/mamba-rocm).

## **Use mamba**
Mamba rocm has already been built to run on Adastra

### **Source mamba**
You can use `bench-ai/Mamba/use/MambaEnv.sh` to get into an environment directly **conducive to use**  mamba rocm :
```bash
source MambaEnv.sh
```

### **Install mamba**

A mamba wheel is already available for installation :
```bash
pip install /lus/work/BCINES/dci/SHARED/bench-ai/Mamba/module_wheel/mamba_ssm-1.2.0.post1-cp311-cp311-linux_x86_64.whl
```
But make sure you have all the required modules.


### **Build mamba**

#### **Easy build**
You can use `bench-ai/Mamba/build/MambaRocmEnvironment.sh` to enter an environment directly **conducive to building** mamba rocm :
```bash
source MambaRocmEnvironment.sh
```
This no-argument script will install all the tools you need to build mamba.
But there are two possible arguments to simplify the task even further:
- **`--build`, `-b`**: Build automatically the mamba rocm wheel installation file
- **`--install`, `-i`**: Build and install automatically mamba rocm

This script does everything written in the manual method automatically.

#### **Manual build**

##### Setup the environment

Start by placing yourself in an empty folder, because we're going to make a mess of things !

###### Setting up the modules
- **`cray-python`**: To get python and be able to use it
- **`torch/2.2.1`**: To get the pythorch module suite
- **`craype-accel-amd-gfx90a`**: To obtain the environment variables that will be used to build to MI250
```bash
module purge

module load cray-python
module load torch/2.2.1
module load craype-accel-amd-gfx90a
```

###### Setting up others environment variables
- **`MAMBA_FORCE_BUILD`**: To force mamba rocm setup to build
- **`NUMEXPR_MAX_THREADS`**: To set the maximum number of threads that NumExpr can use to perform its build calculations. (It is mainly used to avoid a warning)
```bash
export MAMBA_FORCE_BUILD="TRUE"
export NUMEXPR_MAX_THREADS=128
```

###### Use of a python environment for additional modules
A Python environment is required for some additional modules :
- **`packaging`**: This module provides useful features for managing and manipulating Python package metadata.
- **`setuptools`**: This module facilitates the development, packaging and distribution of Python projects.
- **`wheel`**: This module lets you create, install and share Python packages in "wheel" (WHL) format.
- **`triton`**: This module is required for optimization and dynamic model management

If you don't already have a python environment :
```bash
ENV_PATH="./python_environment/MambaRocmEnv"
python3 -m virtualenv "$ENV_PATH"
chmod +x "$ENV_PATH/bin/activate"
source "$ENV_PATH/bin/activate"
```
Then update / install these modules :
```bash
pip install --upgrade pip
python3 -m pip install --upgrade pip
pip install --upgrade pip packaging setuptools wheel triton
```

###### Patch rocm locally
At CINES we have Rocm 6.0, but Mamba Rocm requires Rocm 6.1. The difference between these two versions is two small lines that we're going to modify.
To modify them, we'll have to copy rocm 6.0 locally and patch it locally, the same problem occurs [to install VLLM](https://dci.dci-gitlab.cines.fr/webextranet/software_stack/libraries/index.html#vllm).
```bash
NEW_ROCM_PATH="$(pwd)/$(basename -- "${ROCM_PATH}")"
if [[ ! -e "${NEW_ROCM_PATH}" ]]; then
    cp -rv "$(
        cd -- "${ROCM_PATH}" >/dev/null 2>&1
        pwd -P
    )" "${NEW_ROCM_PATH}" || true
fi
sed -i '93s/.*/'"#define __HOST_DEVICE__ __device__ static"'/' "${NEW_ROCM_PATH}/include/hip/amd_detail/amd_hip_bf16.h"
sed -i '96s/.*/'"#define __HOST_DEVICE__ __host__ __device__ static"'/' "${NEW_ROCM_PATH}/include/hip/amd_detail/amd_hip_bf16.h"
export ROCM_PATH="${NEW_ROCM_PATH}"
export PATH="${ROCM_PATH}/bin:${PATH}"
export HIP_LIB_PATH="${ROCM_PATH}/lib"
export LD_LIBRARY_PATH="${HIP_LIB_PATH}:${LD_LIBRARY_PATH}"
export CMAKE_PREFIX_PATH="${ROCM_PATH}/lib/cmake:${CMAKE_PREFIX_PATH}"
```

##### Install Mamba Rocm

###### Clone [mamba rocm git](https://github.com/EmbeddedLLM/mamba-rocm.git)
```bash
git clone https://github.com/EmbeddedLLM/mamba-rocm.git
```
To evaluate Mamba models, use a specific lm-eval explained [on the original Mamba git](https://github.com/state-spaces/mamba?tab=readme-ov-file#evaluations), available with these commands :
```bash
cd mamba-rocm
git submodule update --init --recursive
cd 3rdparty/lm-evaluation-harness
git checkout big-refactor
pip install -e .
cd ../../..
```

###### Build
After all this preparation we can finally build mamba rocm :
```bash
cd mamba-rocm
python setup.py bdist_wheel --dist-dir=dist
```
The build will take a little while, then you'll have the wheel file built in the dist folder, so all you have to do is install it with this command:

```bash
cd dist
pip install BUILT_WHEEL_FILE.whl
```
Remember to replace BUILT_WHEEL_FILE.whl with the name of the wheel file you have just built.


## **Benchmarks**

Don't forget to change the accounts in the run slrum files.

### **Inference**

The inference benchmark only works for a single GPU at the moment.

**You don't even need to install mamba and prepare an environment yourself. Everything is done for you** in MambaEnv.sh, which is sourced by the run file. So you can be in a completely different environment, the benchmark will be in the mamba environment and won't affect your environment. (It only creates a python_environment folder to contain its modules, including mamba).

```bash
git clone https://depot.cines.fr/dci/rnd/bench-ai.git
cd bench-ai/Mamba/benchmark/inference
ln -s ../../MambaEnv.sh
sbatch run_InferenceBenchmark.sh
```

Some mamba pre-trained models available on HuggingFace are **already downloaded** in the default models folder.
`MambaEnv.sh` installs a build version of the mamba rocm module for Adastra in a python environment named `python_environment/MambaRocmEnv`.
**By default** `benchmark_generation.sh` runs a mamba-130m benchmark with a prompt length of 2048, a generation length of 128 and a batch size of 1.
This slurm command **launches** `GenerationBenchmark.py` **with all the arguments** you give it to `run_InferenceBenchmark.sh`.
But many **parameters can be modified**, as in the original Mamba benchmark :

#### Usage
```bash
usage: InferenceBenchmark.py [-h] [--model-name MODEL_NAME]
                             [--model-path MODEL_PATH] [--prompt PROMPT]
                             [--promptlen PROMPTLEN] [--genlen GENLEN]
                             [--temperature TEMPERATURE] [--topk TOPK]
                             [--topp TOPP] [--minp MINP]
                             [--repetition-penalty REPETITION_PENALTY]
                             [--batch BATCH]
```
#### Options

- **`-h`, `--help`**: Show help message and exit.

- **`--model-name`**: Choose a model from those already downloaded. This option allows you to specify the name of the model you want to use for generation.

- **`--model-path`**: Specify the path to the model file. This option allows you to use a model that is not in the default model directory*.

- **`--prompt`**: Choose a customized prompt. By default, a randomly generated prompt is used. However, you can use this option to provide your own custom prompt.

- **`--promptlen`**: If no custom prompt is provided, you can choose the length of the automatically created prompt. The default length is 2048 characters.

- **`--genlen`**: Choose the length of the generated output. This option allows you to specify how long you want the generated text to be. The default length is 128 characters.

- **`--temperature`**: Choosing the generation temperature. This value controls the randomness of the output. Higher values result in more random outputs, while lower values make the output more deterministic.

- **`--topk`**: This option allows you to implement Top-K sampling, where only the top K most probable next words are considered.

- **`--topp`**: This option allows you to implement Nucleus (Top-P) sampling, where the model considers the smallest possible set of words whose cumulative probability exceeds a certain threshold P.

- **`--minp`**: This option allows you to set a minimum probability for a word to be included in the set of possible next words.

- **`--repetition-penalty`**: This option allows you to set a penalty for repeated words or phrases, which can help prevent the model from generating repetitive text.

- **`--batch`**: This option allows you to specify the batch size for generation. This can be useful for generating multiple outputs at once.

*If you want **more than the pre-downloaded models**, you'll need to download them locally and [upload them to your session](https://dci.dci-gitlab.cines.fr/webextranet/data_storage_and_transfers/index.html#data-transfer), then set the `--model-path` parameter. You can benchmark models other than Mamba rocm with InferenceBenchmark.py to see the difference.

#### **On MI250 64GB (Adastra)**

On a **single** MI250 GPU (64GB) with a prompt length of 2048; a generation length of 128; a batch size of 1 to 128 by multiplication by 2; each result calculated with the average of 3 samples.
Missing data on some models is due to memory overflow.

##### state-spaces/mamba-130m

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 280 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 492 | 2.03ms | 260.39ms | 0ns |
| 2 | AMD Instinct MI250X | 312 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 831 | 1.20ms | 308.24ms | 0ns |
| 4 | AMD Instinct MI250X | 375 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 1456 | 0.69ms | 351.54ms | 0ns |
| 8 | AMD Instinct MI250X | 503 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 2357 | 0.42ms | 434.48ms | 0ns |
| 16 | AMD Instinct MI250X | 762 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 2811 | 0.36ms | 728.65ms | 0ns |
| 32 | AMD Instinct MI250X | 1272 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 3706 | 0.27ms | 1105.18ms | 0ns |
| 64 | AMD Instinct MI250X | 2286 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 4207 | 0.24ms | 1947.06ms | 0ns |
| 128 | AMD Instinct MI250X | 4330 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 4330 | 0.23ms | 3783.90ms | 0ns |

##### state-spaces/mamba-370m

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 752 MB | 2.2.1+rocm5.7 | 0.37B | 2048 | 128 | 220 | 4.54ms | 581.74ms | 0ns |
| 2 | AMD Instinct MI250X | 792 MB | 2.2.1+rocm5.7 | 0.37B | 2048 | 128 | 346 | 2.89ms | 739.65ms | 0ns |
| 4 | AMD Instinct MI250X | 873 MB | 2.2.1+rocm5.7 | 0.37B | 2048 | 128 | 592 | 1.69ms | 864.18ms | 0ns |
| 8 | AMD Instinct MI250X | 1033 MB | 2.2.1+rocm5.7 | 0.37B | 2048 | 128 | 897 | 1.12ms | 1142.00ms | 0ns |
| 16 | AMD Instinct MI250X | 1357 MB | 2.2.1+rocm5.7 | 0.37B | 2048 | 128 | 1027 | 0.97ms | 1993.71ms | 0ns |
| 32 | AMD Instinct MI250X | 1997 MB | 2.2.1+rocm5.7 | 0.37B | 2048 | 128 | 1173 | 0.85ms | 3492.34ms | 0ns |
| 64 | AMD Instinct MI250X | 3284 MB | 2.2.1+rocm5.7 | 0.37B | 2048 | 128 | 1295 | 0.77ms | 6324.83ms | 0ns |
| 128 | AMD Instinct MI250X | 5854 MB | 2.2.1+rocm5.7 | 0.37B | 2048 | 128 | 1315 | 0.76ms | 12455.76ms | 0ns |

##### state-spaces/mamba-790m

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 1572 MB | 2.2.1+rocm5.7 | 0.79B | 2048 | 128 | 160 | 6.26ms | 801.33ms | 0ns |
| 2 | AMD Instinct MI250X | 1627 MB | 2.2.1+rocm5.7 | 0.79B | 2048 | 128 | 263 | 3.80ms | 973.58ms | 0ns |
| 4 | AMD Instinct MI250X | 1735 MB | 2.2.1+rocm5.7 | 0.79B | 2048 | 128 | 415 | 2.41ms | 1233.11ms | 0ns |
| 8 | AMD Instinct MI250X | 1951 MB | 2.2.1+rocm5.7 | 0.79B | 2048 | 128 | 591 | 1.69ms | 1733.10ms | 0ns |
| 16 | AMD Instinct MI250X | 2396 MB | 2.2.1+rocm5.7 | 0.79B | 2048 | 128 | 711 | 1.41ms | 2879.23ms | 0ns |
| 32 | AMD Instinct MI250X | 3249 MB | 2.2.1+rocm5.7 | 0.79B | 2048 | 128 | 723 | 1.38ms | 5663.95ms | 0ns |
| 64 | AMD Instinct MI250X | 4998 MB | 2.2.1+rocm5.7 | 0.79B | 2048 | 128 | 767 | 1.30ms | 10680.81ms | 0ns |
| 128 | AMD Instinct MI250X | 8438 MB | 2.2.1+rocm5.7 | 0.79B | 2048 | 128 | 743 | 1.35ms | 22051.34ms | 0ns |

##### state-spaces/mamba-1.4b

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 2696 MB | 2.2.1+rocm5.7 | 1.37B | 2048 | 128 | 114 | 8.81ms | 1127.15ms | 0ns |
| 2 | AMD Instinct MI250X | 2764 MB | 2.2.1+rocm5.7 | 1.37B | 2048 | 128 | 185 | 5.41ms | 1384.25ms | 0ns |
| 4 | AMD Instinct MI250X | 2899 MB | 2.2.1+rocm5.7 | 1.37B | 2048 | 128 | 286 | 3.50ms | 1793.30ms | 0ns |
| 8 | AMD Instinct MI250X | 3171 MB | 2.2.1+rocm5.7 | 1.37B | 2048 | 128 | 391 | 2.56ms | 2621.82ms | 0ns |
| 16 | AMD Instinct MI250X | 3716 MB | 2.2.1+rocm5.7 | 1.37B | 2048 | 128 | 423 | 2.36ms | 4836.27ms | 0ns |
| 32 | AMD Instinct MI250X | 4802 MB | 2.2.1+rocm5.7 | 1.37B | 2048 | 128 | 327 | 3.05ms | 12509.47ms | 0ns |
| 64 | AMD Instinct MI250X | 6975 MB | 2.2.1+rocm5.7 | 1.37B | 2048 | 128 | 332 | 3.01ms | 24672.30ms | 0ns |
| 128 | AMD Instinct MI250X | 11323 MB | 2.2.1+rocm5.7 | 1.37B | 2048 | 128 | 330 | 3.03ms | 49692.64ms | 0ns |

##### state-spaces/mamba-2.8b

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 5446 MB | 2.2.1+rocm5.7 | 2.77B | 2048 | 128 | 71 | 14.07ms | 1801.40ms | 0ns |
| 2 | AMD Instinct MI250X | 5531 MB | 2.2.1+rocm5.7 | 2.77B | 2048 | 128 | 113 | 8.82ms | 2257.76ms | 0ns |
| 4 | AMD Instinct MI250X | 5701 MB | 2.2.1+rocm5.7 | 2.77B | 2048 | 128 | 167 | 5.98ms | 3061.81ms | 0ns |
| 8 | AMD Instinct MI250X | 6041 MB | 2.2.1+rocm5.7 | 2.77B | 2048 | 128 | 220 | 4.54ms | 4648.56ms | 0ns |
| 16 | AMD Instinct MI250X | 6721 MB | 2.2.1+rocm5.7 | 2.77B | 2048 | 128 | 243 | 4.12ms | 8429.05ms | 0ns |
| 32 | AMD Instinct MI250X | 8079 MB | 2.2.1+rocm5.7 | 2.77B | 2048 | 128 | 223 | 4.49ms | 18383.47ms | 0ns |
| 64 | AMD Instinct MI250X | 10797 MB | 2.2.1+rocm5.7 | 2.77B | 2048 | 128 | 230 | 4.35ms | 35637.58ms | 0ns |
| 128 | AMD Instinct MI250X | 16232 MB | 2.2.1+rocm5.7 | 2.77B | 2048 | 128 | 237 | 4.22ms | 69159.54ms | 0ns |

##### facebook/opt-125m

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 323 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 129 | 7.76ms | 993.30ms | 0ns |
| 2 | AMD Instinct MI250X | 399 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 250 | 3.99ms | 1022.60ms | 0ns |
| 4 | AMD Instinct MI250X | 552 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 434 | 2.31ms | 1181.05ms | 0ns |
| 8 | AMD Instinct MI250X | 858 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 695 | 1.44ms | 1473.21ms | 0ns |
| 16 | AMD Instinct MI250X | 1470 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 975 | 1.03ms | 2100.42ms | 0ns |
| 32 | AMD Instinct MI250X | 2694 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 1227 | 0.82ms | 3338.76ms | 0ns |
| 64 | AMD Instinct MI250X | 5142 MB | 2.2.1+rocm5.7 | 0.13B | 2048 | 128 | 1391 | 0.72ms | 5891.28ms | 0ns |

##### facebook/opt-350m

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 837 MB | 2.2.1+rocm5.7 | 0.33B | 2048 | 128 | 67 | 14.89ms | 1905.91ms | 0ns |
| 2 | AMD Instinct MI250X | 1041 MB | 2.2.1+rocm5.7 | 0.33B | 2048 | 128 | 126 | 7.91ms | 2025.98ms | 0ns |
| 4 | AMD Instinct MI250X | 1448 MB | 2.2.1+rocm5.7 | 0.33B | 2048 | 128 | 212 | 4.71ms | 2410.39ms | 0ns |
| 8 | AMD Instinct MI250X | 2264 MB | 2.2.1+rocm5.7 | 0.33B | 2048 | 128 | 317 | 3.15ms | 3227.41ms | 0ns |
| 16 | AMD Instinct MI250X | 3896 MB | 2.2.1+rocm5.7 | 0.33B | 2048 | 128 | 421 | 2.37ms | 4862.49ms | 0ns |
| 32 | AMD Instinct MI250X | 7159 MB | 2.2.1+rocm5.7 | 0.33B | 2048 | 128 | 503 | 1.99ms | 8136.75ms | 0ns |

##### facebook/opt-1.3b

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 2917 MB | 2.2.1+rocm5.7 | 1.32B | 2048 | 128 | 52 | 19.24ms | 2462.50ms | 0ns |
| 2 | AMD Instinct MI250X | 3325 MB | 2.2.1+rocm5.7 | 1.32B | 2048 | 128 | 88 | 11.36ms | 2909.25ms | 0ns |
| 4 | AMD Instinct MI250X | 4141 MB | 2.2.1+rocm5.7 | 1.32B | 2048 | 128 | 136 | 7.37ms | 3775.78ms | 0ns |
| 8 | AMD Instinct MI250X | 5772 MB | 2.2.1+rocm5.7 | 1.32B | 2048 | 128 | 186 | 5.37ms | 5494.08ms | 0ns |
| 16 | AMD Instinct MI250X | 9035 MB | 2.2.1+rocm5.7 | 1.32B | 2048 | 128 | 227 | 4.41ms | 9035.54ms | 0ns |

##### facebook/opt-2.7b

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 5738 MB | 2.2.1+rocm5.7 | 2.65B | 2048 | 128 | 45 | 22.39ms | 2866.39ms | 0ns |
| 2 | AMD Instinct MI250X | 6418 MB | 2.2.1+rocm5.7 | 2.65B | 2048 | 128 | 71 | 14.12ms | 3614.55ms | 0ns |
| 4 | AMD Instinct MI250X | 7777 MB | 2.2.1+rocm5.7 | 2.65B | 2048 | 128 | 101 | 9.94ms | 5089.52ms | 0ns |
| 8 | AMD Instinct MI250X | 10496 MB | 2.2.1+rocm5.7 | 2.65B | 2048 | 128 | 127 | 7.85ms | 8035.73ms | 0ns |

##### EleutherAI/pythia-160m

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 494 MB | 2.2.1+rocm5.7 | 0.16B | 2048 | 128 | 102 | 9.81ms | 1255.29ms | 0ns |
| 2 | AMD Instinct MI250X | 603 MB | 2.2.1+rocm5.7 | 0.16B | 2048 | 128 | 197 | 5.07ms | 1298.94ms | 0ns |
| 4 | AMD Instinct MI250X | 830 MB | 2.2.1+rocm5.7 | 0.16B | 2048 | 128 | 340 | 2.94ms | 1507.17ms | 0ns |
| 8 | AMD Instinct MI250X | 1289 MB | 2.2.1+rocm5.7 | 0.16B | 2048 | 128 | 538 | 1.86ms | 1902.76ms | 0ns |
| 16 | AMD Instinct MI250X | 2206 MB | 2.2.1+rocm5.7 | 0.16B | 2048 | 128 | 751 | 1.33ms | 2727.87ms | 0ns |
| 32 | AMD Instinct MI250X | 4042 MB | 2.2.1+rocm5.7 | 0.16B | 2048 | 128 | 938 | 1.07ms | 4368.58ms | 0ns |
| 64 | AMD Instinct MI250X | 7715 MB | 2.2.1+rocm5.7 | 0.16B | 2048 | 128 | 1062 | 0.94ms | 7717.32ms | 0ns |

##### EleutherAI/pythia-410m

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 1200 MB | 2.2.1+rocm5.7 | 0.41B | 2048 | 128 | 53 | 19.00ms | 2432.42ms | 0ns |
| 2 | AMD Instinct MI250X | 1501 MB | 2.2.1+rocm5.7 | 0.41B | 2048 | 128 | 97 | 10.32ms | 2641.28ms | 0ns |
| 4 | AMD Instinct MI250X | 2112 MB | 2.2.1+rocm5.7 | 0.41B | 2048 | 128 | 161 | 6.19ms | 3171.18ms | 0ns |
| 8 | AMD Instinct MI250X | 3337 MB | 2.2.1+rocm5.7 | 0.41B | 2048 | 128 | 242 | 4.14ms | 4235.94ms | 0ns |
| 16 | AMD Instinct MI250X | 5783 MB | 2.2.1+rocm5.7 | 0.41B | 2048 | 128 | 319 | 3.13ms | 6412.95ms | 0ns |
| 32 | AMD Instinct MI250X | 10678 MB | 2.2.1+rocm5.7 | 0.41B | 2048 | 128 | 379 | 2.64ms | 10802.06ms | 0ns |

##### EleutherAI/pythia-1b

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 2428 MB | 2.2.1+rocm5.7 | 1.01B | 2048 | 128 | 67 | 14.90ms | 1907.54ms | 0ns |
| 2 | AMD Instinct MI250X | 2835 MB | 2.2.1+rocm5.7 | 1.01B | 2048 | 128 | 114 | 8.79ms | 2251.37ms | 0ns |
| 4 | AMD Instinct MI250X | 3651 MB | 2.2.1+rocm5.7 | 1.01B | 2048 | 128 | 181 | 5.53ms | 2829.72ms | 0ns |
| 8 | AMD Instinct MI250X | 5282 MB | 2.2.1+rocm5.7 | 1.01B | 2048 | 128 | 254 | 3.94ms | 4031.71ms | 0ns |
| 16 | AMD Instinct MI250X | 8545 MB | 2.2.1+rocm5.7 | 1.01B | 2048 | 128 | 313 | 3.20ms | 6550.59ms | 0ns |
| 32 | AMD Instinct MI250X | 15071 MB | 2.2.1+rocm5.7 | 1.01B | 2048 | 128 | 349 | 2.86ms | 11726.06ms | 0ns |

##### EleutherAI/pythia-1.4b

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 3433 MB | 2.2.1+rocm5.7 | 1.41B | 2048 | 128 | 45 | 22.10ms | 2828.71ms | 0ns |
| 2 | AMD Instinct MI250X | 4043 MB | 2.2.1+rocm5.7 | 1.41B | 2048 | 128 | 76 | 13.17ms | 3370.53ms | 0ns |
| 4 | AMD Instinct MI250X | 5266 MB | 2.2.1+rocm5.7 | 1.41B | 2048 | 128 | 119 | 8.43ms | 4315.90ms | 0ns |
| 8 | AMD Instinct MI250X | 7718 MB | 2.2.1+rocm5.7 | 1.41B | 2048 | 128 | 164 | 6.11ms | 6253.88ms | 0ns |
| 16 | AMD Instinct MI250X | 12608 MB | 2.2.1+rocm5.7 | 1.41B | 2048 | 128 | 198 | 5.05ms | 10336.03ms | 0ns |

##### EleutherAI/pythia-2.8b

| Batch size | GPU | Memory Allocated | PyTorch | Parameters | Prompt length | Generation length | Throughput | Time per token | Inference time | Samples variance|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | AMD Instinct MI250X | 6487 MB | 2.2.1+rocm5.7 | 2.78B | 2048 | 128 | 35 | 28.98ms | 3709.07ms | 0ns |
| 2 | AMD Instinct MI250X | 7508 MB | 2.2.1+rocm5.7 | 2.78B | 2048 | 128 | 54 | 18.36ms | 4700.96ms | 0ns |
| 4 | AMD Instinct MI250X | 9547 MB | 2.2.1+rocm5.7 | 2.78B | 2048 | 128 | 78 | 12.86ms | 6584.62ms | 0ns |
| 8 | AMD Instinct MI250X | 13623 MB | 2.2.1+rocm5.7 | 2.78B | 2048 | 128 | 99 | 10.13ms | 10377.95ms | 0ns |

#### **On A100 80GB ([Mamba Paper](https://arxiv.org/pdf/2312.00752))**
(Page 16 figure 8 Right; Page 36 End-to-end Inference)

##### state-spaces/mamba-1.4b
| Batch size | GPU | Parameters | Prompt length | Generation length | Throughput |
|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | Nvidia A100 | 1.37B | 2048 | 128 | 140 |
| 2 | Nvidia A100 | 1.37B | 2048 | 128 | 247 |
| 4 | Nvidia A100 | 1.37B | 2048 | 128 | 441 |
| 8 | Nvidia A100 | 1.37B | 2048 | 128 | 744 |
| 16 | Nvidia A100 | 1.37B | 2048 | 128 | 1089 |
| 32 | Nvidia A100 | 1.37B | 2048 | 128 | 1445 |
| 64 | Nvidia A100 | 1.37B | 2048 | 128 | 1688 |
| 128 | Nvidia A100 | 1.37B | 2048 | 128 | 1814 |
