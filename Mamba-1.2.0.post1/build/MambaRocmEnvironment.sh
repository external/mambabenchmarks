#!/bin/bash

if [ "$0" = "-bash" ]; then
    last_command=$(history 1 | sed 's/^[ ]*[0-9]*[ ]*//')
else
    last_command="$0 $*"
fi

function setup_modules() {
  echo "Setting up the modules... (cray-python, torch/2.2.1, cpe/23.12, rocm/5.7.1, craype-accel-amd-gfx90a)"
  module purge
  module load cray-python
  module load torch/2.2.1
  module load cpe/23.12
  module load rocm/5.7.1
  module load craype-accel-amd-gfx90a
}

function setup_clean_workspace() {
  echo "Creating a clean workspace... (Moving into the MambaRocm folder)"
  if [ "$(basename "$(pwd)")" != "MambaRocm" ]; then
    if [ ! -d "MambaRocm" ]; then
      mkdir MambaRocm
    fi
    cd MambaRocm
    mv "../$BASH_SOURCE" "./$BASH_SOURCE"
  fi
}

function build_and_install_mamba_wheel_if_flag_set() {
  if echo $last_command | grep --quiet "\-\-build" || echo $last_command | grep --quiet "\-b" || echo $last_command | grep --quiet "\-\-install" || echo $last_command | grep --quiet "\-i"; then
    echo "Building the mamba wheel installation file... (Output to mamba-rocm/dist)"
    cd mamba-rocm
    python setup.py bdist_wheel --dist-dir=dist
    wheels=(dist/*.whl)
    if echo $last_command | grep --quiet "\-\-install" || echo $last_command | grep --quiet "\-i"; then
      if [ ${#wheels[@]} -ne 1 ]; then
        echo "Warning: there are multiple wheels in the dist directory, cannot determine which one to install."
        echo "Please use one of the following commands to install one of the built versions of Mamba:"
        for wheel in "${wheels[@]}"; do
          echo ">pip install mamba-rocm/$wheel"
        done
        echo
        echo "Mamba rocm built successfully."
        cd ../..
      else
        wheel=$(basename "${wheels[0]}")
        echo "Installation from the built wheel file... (mamba-rocm/dist/$wheel)"
        cd dist
        pip install $wheel
        cd ../..
        echo
        echo "Mamba rocm built and installed successfully."
      fi
    else
      echo "Mamba is ready to install :"
      for wheel in "${wheels[@]}"; do
        echo ">pip install mamba-rocm/$wheel"
      done
      cd ../..
      echo
      echo "Mamba rocm built successfully."
    fi
  else
    echo
    if ! pip list --format=freeze 2>/dev/null | grep -q 'mamba_ssm=='; then
        echo "Mamba is ready to build (cd mamba-rocm && python setup.py bdist_wheel --dist-dir=dist)"
    fi
    if [ "$0" = "-bash" ]; then
        echo "Successful preparation, you're in the ideal environment for mamba rocm !"
      fi
  fi
}

function setup_rocm() {
  NEW_ROCM_PATH="$(pwd)/$(basename -- "${ROCM_PATH}")"
  echo "Copy rocm for a local fix of the module... ($ROCM_PATH to $NEW_ROCM_PATH)"
  if [[ ! -e "${NEW_ROCM_PATH}" ]]; then
      cp -rv "$(
          cd -- "${ROCM_PATH}" >/dev/null 2>&1
          pwd -P
      )" "${NEW_ROCM_PATH}" || true
  fi
  echo "Modification of the local version of rocm... (Add 'static' to line 93 and 96 of the amd_hip_bf16.h file)"
  sed -i '93s/.*/'"#define __HOST_DEVICE__ __device__ static"'/' "${NEW_ROCM_PATH}/include/hip/amd_detail/amd_hip_bf16.h"
  sed -i '96s/.*/'"#define __HOST_DEVICE__ __host__ __device__ static"'/' "${NEW_ROCM_PATH}/include/hip/amd_detail/amd_hip_bf16.h"
  echo "Changing rocm environment variables... (ROCM_PATH, PATH, HIP_LIB_PATH, LD_LIBRARY_PATH, CMAKE_PREFIX_PATH)"
  export ROCM_PATH="${NEW_ROCM_PATH}"
  export PATH="${ROCM_PATH}/bin:${PATH}"
  export HIP_LIB_PATH="${ROCM_PATH}/lib"
  export LD_LIBRARY_PATH="${HIP_LIB_PATH}:${LD_LIBRARY_PATH}"
  export CMAKE_PREFIX_PATH="${ROCM_PATH}/lib/cmake:${CMAKE_PREFIX_PATH}"
}

function update_python_env() {
  echo "Use of a python environment... (python_environment/MambaRocmEnv)"
  ENV_PATH="./python_environment/MambaRocmEnv"
  if [ -d "$ENV_PATH" ]; then
    source "$ENV_PATH/bin/activate"
  else
    echo "Creating the python environment..."
    python3 -m pip install --user --upgrade pip
    pip3 install --user --upgrade virtualenv
    python3 -m virtualenv "$ENV_PATH"
    chmod +x "$ENV_PATH/bin/activate"
    source "$ENV_PATH/bin/activate"
  fi
  pip install --upgrade pip
  python3 -m pip install --upgrade pip
  pip install --upgrade pip packaging setuptools wheel triton pytorch-memlab
}

function set_environment_variables() {
  echo "Setting up new environment variables... (MAMBA_FORCE_BUILD,NUMEXPR_MAX_THREADS)"
  export MAMBA_FORCE_BUILD="TRUE"
  export NUMEXPR_MAX_THREADS=128
  export HIP_ARCH="gfx90a"
}

function clone_mamba_repo() {
  if [ ! -d "./mamba-rocm" ]; then
    echo "Cloning the mamba rocm git... (https://github.com/EmbeddedLLM/mamba-rocm.git)"
    git clone https://github.com/EmbeddedLLM/mamba-rocm.git
    cd mamba-rocm
    echo "Mamaba rocm lm-eval installation... (git submodule)"
    git submodule update --init --recursive
    cd 3rdparty/lm-evaluation-harness
    git checkout big-refactor
    pip install -e .
    cd ../../..
  fi
}


function get_help() {
  if (echo $last_command | grep --quiet "\-\-help" || echo $last_command | grep --quiet "\-h") || [ ! "$0" = "-bash" ]; then
    echo "Usage: source $(basename $last_command) [options]"
    echo ""
    echo "Options:"
    echo "  --help, -h  Show this help message and exit"
    echo "  --build, -b  Build the mamba rocm wheel installation file"
    echo "  --install, -i  Build and install mamba rocm"
    echo ""
    echo "Example: source $(basename $last_command)  This will prepare an ideal environment for mamba rocm."
    echo "Example: source $(basename $last_command) --install  This will prepare an ideal environment for mamba rocm, then build the mamba rocm wheel installation file and install it."
    echo ""
    end
  fi
}

function start(){
  set -eu
}

function end(){
  set +eu
}

function main() {
  start
  get_help
  setup_clean_workspace
  setup_modules
  set_environment_variables
  update_python_env
  setup_rocm
  clone_mamba_repo
  build_and_install_mamba_wheel_if_flag_set
  end
}

main
